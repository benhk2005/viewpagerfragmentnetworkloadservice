package com.benleungcreative.vpfragmentloadnetwork;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class TestActivityB extends AppCompatActivity {

    private TextView mainTextView;
    private int mIndex;
    private MyCenterService.FakeDataClass fakeDataClass;

    MyCenterService mService;
    boolean mBound = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        mainTextView = findViewById(R.id.mainTextView);

    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, MyCenterService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(serviceConnection);
    }

    private void updateUIByFakeData(){
        if(fakeDataClass == null || fakeDataClass.status == MyCenterService.Status.LOADING){
            mainTextView.setText("null");
        } else {
            mainTextView.setText(fakeDataClass.data);
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyCenterService.LocalBinder binder = (MyCenterService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            fakeDataClass = mService.getDataClassAtIndex(mIndex);
            updateUIByFakeData();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };
}
