package com.benleungcreative.vpfragmentloadnetwork;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MyCenterService extends Service {

    private final IBinder binder = new LocalBinder();


    private ArrayList<Integer> timeToResponse = new ArrayList<>(10);

    private SparseArray<FakeDataClass> fakeDataClassSparseArray = new SparseArray<>();

    public class LocalBinder extends Binder {
        MyCenterService getService() {
            // Return this instance of LocalService so clients can call public methods
            return MyCenterService.this;
        }



    }

    public MyCenterService() {
        Random random = new Random();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 5, 30, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(80));
        for(int i = 0; i < 10; i++){
            if(fakeDataClassSparseArray.get(i) == null) {
                timeToResponse.add(5 + random.nextInt(10));
                fakeDataClassSparseArray.append(i, new FakeDataClass());
                new FakeNetwork().executeOnExecutor(threadPoolExecutor, i, timeToResponse.get(i));
                fakeDataClassSparseArray.get(i).status = Status.LOADING;
            }
//            new FakeNetwork().execute(i, timeToResponse.get(i));
        }
        Log.d("ttr", TextUtils.join(",", timeToResponse));

    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("onBind", intent.getClass().getSimpleName());
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("onUnbind", intent.getClass().getSimpleName());
        return super.onUnbind(intent);
    }

    FakeDataClass getDataClassAtIndex(int index){
        return fakeDataClassSparseArray.get(index);
    }

    public class FakeNetwork extends AsyncTask<Integer, Integer, String>{

        private int index;
        private int timeToResponse;

        @Override
        protected String doInBackground(Integer... voids) {
            index = voids[0];
            timeToResponse = voids[1];
            try {
                Thread.sleep(timeToResponse*1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return UUID.randomUUID().toString();
        }

        @Override
        protected void onPostExecute(String string) {
            super.onPostExecute(string);
            Log.d("FakeNetwork", String.format("Task #%d download completed.", index));
            fakeDataClassSparseArray.get(index).status = MyCenterService.Status.LOADED;
            fakeDataClassSparseArray.get(index).data = string;
            EventBus.getDefault().post(new ServiceDownloadedEvent(index));
        }
    }

    public static class FakeDataClass {
        public Status status = Status.NOTLOADED;
        public String data;

    }

    public static enum Status{
        NOTLOADED, LOADING, LOADED
    }

}
