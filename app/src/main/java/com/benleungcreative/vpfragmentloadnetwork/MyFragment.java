package com.benleungcreative.vpfragmentloadnetwork;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MyFragment extends Fragment implements LifecycleObserver {

    MyCenterService mService;
    boolean mBound = false;

    public static final String EXTRA_INDEX = "EXTRA_INDEX";

    private int mIndex = 0;

    private TextView mainTextView;
    private ProgressBar mainProgressBar;
    private View mainView;

    private MyCenterService.FakeDataClass fakeDataClass;

    public static MyFragment newInstance(int index){
        MyFragment myFragment = new MyFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(EXTRA_INDEX, index);
        myFragment.setArguments(arguments);
        return myFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null){
            mIndex = getArguments().getInt(EXTRA_INDEX, 0);
        }else{
            mIndex = savedInstanceState.getInt(EXTRA_INDEX);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        context.bindService(new Intent(context, MyCenterService.class), serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getContext().unbindService(serviceConnection);
        mBound = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mBound) {
            fakeDataClass = mService.getDataClassAtIndex(mIndex);
            updateUIByFakeData();
        }
        EventBus.getDefault().register(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(mBound) {
            fakeDataClass = mService.getDataClassAtIndex(mIndex);
            updateUIByFakeData();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onServiceDownloadedEvent(ServiceDownloadedEvent event){
        if(event.index == mIndex) {
            fakeDataClass = mService.getDataClassAtIndex(mIndex);
            updateUIByFakeData();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mainTextView = view.findViewById(R.id.mainTextView);
        mainProgressBar = view.findViewById(R.id.mainProgressBar);
        mainTextView.setText(String.valueOf(mIndex+1));
        mainView = view.findViewById(R.id.mainView);
        mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), TestActivityB.class));
            }
        });
        updateUIByFakeData();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_INDEX, mIndex);
    }

    private void updateUIByFakeData(){
        if(fakeDataClass == null || fakeDataClass.status == MyCenterService.Status.LOADING || fakeDataClass.status == MyCenterService.Status.NOTLOADED){
            mainProgressBar.setVisibility(View.VISIBLE);
        } else {
            mainProgressBar.setVisibility(View.GONE);
            mainTextView.setText(fakeDataClass.data);
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onFragmentResume(){
        Log.d("onFragmentResume", String.valueOf(mIndex));
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onFragmentStart(){
        Log.d("onFragmentStart", String.valueOf(mIndex));
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyCenterService.LocalBinder binder = (MyCenterService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            fakeDataClass = mService.getDataClassAtIndex(mIndex);
            updateUIByFakeData();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

}
